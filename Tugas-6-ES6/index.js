//soal 1
const luas = (x,y) => {
    return x*y;
}
const keliling = (x,y) => {
    return 2*(x+y);
}

console.log("luas = " + luas(2,4) + " dan keliling = "+ keliling(2,4))


//soal 2
const newFunction = (firstName, lastName) => {
  return firstName+ " "+lastName
}

console.log(newFunction("William", "Imoh"))  


//soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName, address, hobby}=newObject

console.log(firstName, lastName, address, hobby)


//soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]

console.log(combined)


//soal 5
const planet = "earth" 
const view = "glass" 
const before = `${"Lorem"} ${view} ${"dolor sit amet, "} ${"consectetur adipiscing elit, "} ${planet}` 

console.log(before)