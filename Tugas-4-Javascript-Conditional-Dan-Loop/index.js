// soal 1
var nilai = 80
if(nilai >= 85){
    console.log("A")
}  else if (nilai >= 75 || nilai < 85){
    console.log("B")
} else if (nilai >= 65 || nilai < 75){
    console.log("C")
} else if (nilai >= 55 || nilai < 65){
    console.log("D")
} else if (nilai < 55){
    console.log("E")
} // Jawaban soal 1 = B

// soal 2
var tanggal = 15;
var bulan = 6;
var tahun = 2003;
var tahunout; var bulanout; var tanggalout;
switch(tanggal) {
    case 1:    tanggalout = '1'; break; 
    case 2:    tanggalout = '2'; break; 
    case 3:    tanggalout = '3'; break; 
    case 4:    tanggalout = '4'; break; 
    case 5:    tanggalout = '5'; break; 
    case 6:    tanggalout = '6'; break; 
    case 7:    tanggalout = '7'; break; 
    case 8:    tanggalout = '8'; break; 
    case 9:    tanggalout = '9'; break; 
    case 10:    tanggalout = '10'; break; 
    case 11:    tanggalout = '11'; break; 
    case 12:    tanggalout = '12'; break; 
    case 13:    tanggalout = '13'; break; 
    case 14:    tanggalout = '14'; break; 
    case 15:    tanggalout = '15'; break; 
    case 16:    tanggalout = '16'; break; 
    case 17:    tanggalout = '17'; break; 
    case 18:    tanggalout = '18'; break; 
    case 19:    tanggalout = '19'; break; 
    case 20:    tanggalout = '20'; break; 
    case 21:    tanggalout = '21'; break; 
    case 22:    tanggalout = '22'; break; 
    case 23:    tanggalout = '23'; break; 
    case 24:    tanggalout = '24'; break; 
    case 25:    tanggalout = '25'; break; 
    case 26:    tanggalout = '26'; break; 
    case 27:    tanggalout = '27'; break; 
    case 28:    tanggalout = '28'; break; 
    case 29:    tanggalout = '29'; break; 
    case 30:    tanggalout = '30'; break; 
}  
    
    switch(bulan) {
        case 1:    bulanout = ' Januari'; break; 
        case 2:    bulanout = ' Februari'; break; 
        case 3:    bulanout = ' Maret'; break; 
        case 4:    bulanout = ' April'; break; 
        case 5:    bulanout = ' Mei'; break; 
        case 6:    bulanout = ' Juni'; break; 
        case 7:    bulanout = ' Juli'; break; 
        case 8:    bulanout = ' Agustus'; break; 
        case 9:    bulanout = ' September'; break; 
        case 10:    bulanout = ' Oktober'; break; 
        case 11:    bulanout = ' November'; break; 
        case 12:    bulanout = ' Desember'; break
    }  

        switch(tahun) {
            case 2000:   tahunout = ' 2000'; break;
            case 2001:   tahunout = ' 2001'; break;
            case 2002:   tahunout = ' 2002'; break;
            case 2003:   tahunout = ' 2003'; break;
            case 2004:   tahunout = ' 2004'; break;
            case 2005:   tahunout = ' 2005'; break;
        }
            console.log(tanggalout.concat(bulanout.concat(tahunout)))

// soal 3
var n=3;
for (var i = 1; i<= n; i++){
    console.log('#'.repeat(i));
}

// soal 4
//saya masih membuat bagian angka sebelum - dengan urutan
//sehingga '===' masuk dalam urutan, jadi angka untuk setiap kalimat tidak sama seperti di contoh:)
var deret = 7;

for (var i = 1; i<=deret+3; i++)
{
    if (i%2 == 0 && i%4 != 0) 
    {
        console.log (i + " - I love Javascript")
    }
    else if (i%2 == 0 && i%4 == 0)
    {
        console.log ("=".repeat(i-1))
    }
    else if (i%2 == 1 && i%4 == 1)
    {
        console.log (i + " - I love programming");
    }
    else 
    {
        console.log (i + " - I love VueJS")
    }
}
