//soal 1 
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var kalimat1 = pertama.substring(0,4)
var kalimat2 = pertama.substring(11,19)
var kalimat3 = kedua.substring(0,8)
var kalimat4 = kedua.substring(8,18)
var kalimat4besar = kalimat4.toLocaleUpperCase()
//jawaban soal 1 = saya senang belajar JAVASCRIPT
console.log(kalimat1.concat(kalimat2.concat(kalimat3.concat(kalimat4besar))))

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var kata1 = parseInt(kataPertama) // parseInt = ubah data dari string ke integer
var kata2 = parseInt(kataKeempat)
//jawaban soal 2 = 24
console.log(kata1+(kataKedua*kataKetiga)+kata2)

//soal 3 
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);
//jawaban soal 3 Kata Pertama: wah; Kata Kedua: javascript; Kata Ketiga: itu; Kata Keempat: keren; Kata Kelima: sekali
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
